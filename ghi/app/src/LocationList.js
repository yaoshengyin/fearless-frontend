import React, { useEffect, useState } from 'react';

function LocationList() {
    const [locations, setLocations] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const url = 'http://localhost:8000/api/locations/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setLocations(data.locations);
            }
        };

        fetchData();
    }, []);

    const handleDelete = async (locationId) => {
        const userConfirmation = window.confirm("Are you sure you want to delete this?");
        if (!userConfirmation) {
            return;
        }
    
        const url = `http://localhost:8000/api/locations/${locationId}`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
    
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setLocations(prevLocations => prevLocations.filter(location => location.id !== locationId));
        }
    }
    

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Location List</h1>
                    <ul>
                    {locations.map(location => (
                        <li key={location.id}>
                            {location.name}, {location.city}, 
                            {location.state && location.state.abbreviation ? location.state.abbreviation : "N/A"}
                            <button className="btn btn-danger ml-4" onClick={() => handleDelete(location.id)}>
                                <i className="fa fa-trash"></i> Delete
                            </button>
                        </li>
                    ))}
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default LocationList;
