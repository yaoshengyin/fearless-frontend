import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import LocationList from './LocationList';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <Router>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/attendees/new" element={<AttendConferenceForm />} />
          <Route path="/locations" element={<LocationList />} />
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="/presentations/new" element={<PresentationForm />} />
          <Route index element={<MainPage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
