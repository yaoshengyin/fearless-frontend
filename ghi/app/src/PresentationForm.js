import React, { useState, useEffect } from 'react';

function PresentationForm() {
  const [formData, setFormData] = useState({
    presenter_name: '',
    presenter_email: '',
    company_name: '',
    title: '',
    synopsis: '',
    conference: ''
  });
  const [conferences, setConferences] = useState([]);

  useEffect(() => {
    const fetchConferences = async () => {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
      }
    };
    fetchConferences();
  }, []);

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const conferenceId = formData.conference;
    const locationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
    const response = await fetch(locationUrl, {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    if (response.ok) {
      setFormData({
        presenter_name: '',
        presenter_email: '',
        company_name: '',
        title: '',
        synopsis: '',
        conference: ''
      });
      const newConference = await response.json();
      console.log(newConference);
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input 
                  type="text" 
                  name="presenter_name" 
                  className="form-control"
                  placeholder="Presenter name"
                  required
                  value={formData.presenter_name}
                  onChange={handleChange}
                />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>

              <div className="form-floating mb-3">
                <input 
                  type="email" 
                  name="presenter_email" 
                  className="form-control"
                  placeholder="Presenter email"
                  required
                  value={formData.presenter_email}
                  onChange={handleChange}
                />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>

              <div className="form-floating mb-3">
                <input 
                  type="text" 
                  name="company_name" 
                  className="form-control"
                  placeholder="Company name"
                  value={formData.company_name}
                  onChange={handleChange}
                />
                <label htmlFor="company_name">Company name</label>
              </div>

              <div className="form-floating mb-3">
                <input 
                  type="text" 
                  name="title" 
                  className="form-control"
                  placeholder="Title"
                  required
                  value={formData.title}
                  onChange={handleChange}
                />
                <label htmlFor="title">Title</label>
              </div>

              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea 
                  name="synopsis" 
                  className="form-control"
                  rows="3"
                  value={formData.synopsis}
                  onChange={handleChange}
                />
              </div>

              <div className="mb-3">
                <select 
                  required 
                  name="conference" 
                  className="form-select"
                  value={formData.conference} 
                  onChange={handleChange}
                >
                  <option value="">Choose a conference</option>
                  {conferences.map((conference) => (
                    <option key={conference.id} value={conference.id}>
                      {conference.name}
                    </option>
                  ))}
                </select>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
