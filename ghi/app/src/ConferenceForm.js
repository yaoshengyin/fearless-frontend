import React, { useState, useEffect } from 'react';

function ConferenceForm() {
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [maxPresentations, setMaxPresentations] = useState('');
  const [maxAttendees, setMaxAttendees] = useState('');
  const [selectedLocation, setSelectedLocation] = useState('');

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const formData = {
      name,
      starts,
      ends,
      description,
      max_presentations: maxPresentations,
      max_attendees: maxAttendees,
      location: selectedLocation,
    };
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
    }
  };


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="name"
                placeholder="Conference Name"
                required
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="date"
                className="form-control"
                id="starts"
                placeholder="Starts"
                required
                value={starts}
                onChange={(e) => setStarts(e.target.value)}
              />
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="date"
                className="form-control"
                id="ends"
                placeholder="Ends"
                required
                value={ends}
                onChange={(e) => setEnds(e.target.value)}
              />
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="form-floating mb-3">
              <textarea
                className="form-control"
                id="description"
                placeholder="Description"
                required
                rows="3"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              ></textarea>
              <label htmlFor="description">Description</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="number"
                className="form-control"
                id="max_presentations"
                placeholder="Max Presentations"
                required
                value={maxPresentations}
                onChange={(e) => setMaxPresentations(e.target.value)}
              />
              <label htmlFor="max_presentations">Max presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="number"
                className="form-control"
                id="max_attendees"
                placeholder="Max Attendees"
                required
                value={maxAttendees}
                onChange={(e) => setMaxAttendees(e.target.value)}
              />
              <label htmlFor="max_attendees">Max attendees</label>
            </div>
            <div className="mb-3">
              <select
                className="form-select"
                id="location"
                required
                value={selectedLocation}
                onChange={(e) => setSelectedLocation(e.target.value)}
              >
                <option value="">Choose a location</option>
                {locations.map((location) => (
                  <option key={location.id} value={location.id}>
                    {location.name}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
